resource "aws_elasticache_replication_group" "default" {
  replication_group_id          = var.name
  replication_group_description = "ElastiCache replication group for ${var.name}"
  engine                        = "redis"
  number_cache_clusters         = var.cluster_mode_enabled ? null : var.cluster_size
  automatic_failover_enabled    = var.automatic_failover_enabled
  auto_minor_version_upgrade    = var.auto_minor_version_upgrade
  availability_zones            = var.cluster_mode_enabled ? null : slice(var.availability_zones, 0, var.cluster_size)
  at_rest_encryption_enabled    = var.at_rest_encryption_enabled
  kms_key_id                    = var.kms_key_id
  transit_encryption_enabled    = var.transit_encryption_enabled
  auth_token                    = var.transit_encryption_enabled ? var.auth_token : null
  notification_topic_arn        = var.notification_topic_arn
  parameter_group_name          = aws_elasticache_parameter_group.default.name
  subnet_group_name             = aws_elasticache_subnet_group.default.name
  security_group_ids            = var.use_existing_security_groups ? var.existing_security_groups : [join("", aws_security_group.default.*.id)]
  multi_az_enabled              = var.multi_az_enabled
  node_type                     = var.node_type
  engine_version                = var.engine_version
  port                          = var.port
  maintenance_window            = var.maintenance_window
  snapshot_window               = var.snapshot_window
  snapshot_retention_limit      = var.snapshot_retention_limit
  snapshot_name                 = var.snapshot_name
  apply_immediately             = var.apply_immediately
  tags = merge(
    {
      "Name" = var.name
    },
    var.tags,
  )

  dynamic "cluster_mode" {
    for_each = var.cluster_mode_enabled ? ["true"] : []
    content {
      replicas_per_node_group = var.cluster_mode_replicas_per_node_group
      num_node_groups         = var.cluster_mode_num_node_groups
    }
  }
}

resource "aws_elasticache_parameter_group" "default" {
  name        = var.name
  family      = var.family
  description = "ElastiCache replication group for ${var.name}"
  tags = merge(
    {
      "Name" = var.name
    },
    var.tags,
  )
  dynamic "parameter" {
    for_each = var.cluster_mode_enabled ? concat([{ "name" = "cluster-enabled", "value" = "yes" }], var.parameter) : var.parameter
    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
}

resource "aws_elasticache_subnet_group" "default" {
  name        = var.name
  subnet_ids  = var.subnet_ids
  description = "ElastiCache replication group for ${var.name}"
  tags = merge(
    {
      "Name" = var.name
    },
    var.tags,
  )
}

resource "aws_security_group" "default" {
  count  = var.use_existing_security_groups == false ? 1 : 0
  name   = "${var.name}-elasticache-redis"
  vpc_id = var.vpc_id
  tags = merge(
    {
      "Name" = "${var.name}-elasticache-redis"
    },
    var.tags,
  )
}

resource "aws_security_group_rule" "egress" {
  count             = var.use_existing_security_groups == false ? 1 : 0
  description       = "Allow all egress traffic"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = join("", aws_security_group.default.*.id)
  type              = "egress"
}

resource "aws_security_group_rule" "ingress_security_groups" {
  count                    = var.use_existing_security_groups == false ? length(var.allowed_security_groups) : 0
  description              = "Allow inbound traffic from existing Security Groups"
  from_port                = var.port
  to_port                  = var.port
  protocol                 = "tcp"
  source_security_group_id = var.allowed_security_groups[count.index]
  security_group_id        = join("", aws_security_group.default.*.id)
  type                     = "ingress"
}

resource "aws_security_group_rule" "ingress_cidr_blocks" {
  count             = var.use_existing_security_groups == false && length(var.allowed_cidr_blocks) > 0 ? 1 : 0
  description       = "Allow inbound traffic from CIDR blocks"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr_blocks
  security_group_id = join("", aws_security_group.default.*.id)
  type              = "ingress"
}
