variable "name" {
  description = "(Required) The replication group identifier. This parameter is stored as a lowercase string."
  type        = string
}

variable "number_cache_clusters" {
  description = "(Required for Cluster Mode Disabled) The number of cache clusters (primary and replicas) this replication group will have. If Multi-AZ is enabled, the value of this parameter must be at least 2. Updates will occur before other modifications."
  type        = number
  default     = null
}

variable "node_type" {
  description = "(Required) The compute and memory capacity of the nodes in the node group."
  type        = string
  default     = null
}

variable "automatic_failover_enabled" {
  description = "(Optional) Specifies whether a read-only replica will be automatically promoted to read/write primary if the existing primary fails. If true, Multi-AZ is enabled for this replication group. If false, Multi-AZ is disabled for this replication group. Must be enabled for Redis (cluster mode enabled) replication groups."
  type        = bool
  default     = false
}

variable "at_rest_encryption_enabled" {
  description = "(Optional) Whether to enable encryption at rest."
  type        = bool
  default     = null
}

variable "transit_encryption_enabled" {
  description = " (Optional) Whether to enable encryption in transit."
  type        = bool
  default     = null
}

variable "auth_token" {
  description = "(Optional) The password used to access a password protected server. Can be specified only if transit_encryption_enabled = true."
  type        = string
  default     = null
}

variable "kms_key_id" {
  description = "(Optional) The ARN of the key that you wish to use if encrypting at rest. If not supplied, uses service managed encryption. Can be specified only if at_rest_encryption_enabled = true"
  type        = string
  default     = null
}

variable "subnet_ids" {
  description = "(Required) List of VPC Subnet IDs for the cache subnet group."
  type        = list(string)
  default     = []
}

variable "vpc_id" {
  description = "(Required) VPC Id to associate with Redis ElastiCache."
  type        = string
}

variable "use_existing_security_groups" {
  description = "Flag to enable/disable creation of Security Group in the module. Set to `true` to disable Security Group creation and provide a list of existing security Group IDs in `existing_security_groups` to place the cluster into"
  type        = bool
  default     = false
}

variable "existing_security_groups" {
  description = "List of existing Security Group IDs to place the cluster into. Set `use_existing_security_groups` to `true` to enable using `existing_security_groups` as Security Groups for the cluster"
  type        = list(string)
  default     = []
}

variable "allowed_security_groups" {
  description = "List of Security Group IDs that are allowed ingress to the cluster's Security Group created in the module"
  type        = list(string)
  default     = []
}

variable "allowed_cidr_blocks" {
  description = "List of CIDR blocks that are allowed ingress to the cluster's Security Group created in the module"
  type        = list(string)
  default     = []
}

variable "engine_version" {
  description = "(Optional) The version number of the cache engine to be used for the cache clusters in this replication group."
  type        = string
  default     = "5.0.6"
}

variable "port" {
  description = "(Optional) The port number on which each of the cache nodes will accept connections."
  type        = number
  default     = 6379
}

variable "maintenance_window" {
  description = "(Optional) Specifies the weekly time range for when maintenance on the cache cluster is performed. The format is ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period. Example: sun:05:00-sun:09:00"
  type        = string
  default     = "wed:03:00-wed:04:00"
}

variable "notification_topic_arn" {
  description = "(Optional) An Amazon Resource Name (ARN) of an SNS topic to send ElastiCache notifications to."
  type        = string
  default     = null
}

variable "snapshot_window" {
  description = "(Optional, Redis only) The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster. The minimum snapshot window is a 60 minute period."
  type        = string
  default     = "06:30-07:30"
}

variable "snapshot_retention_limit" {
  description = "(Optional, Redis only) The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them. "
  type        = number
  default     = "0"
}

variable "snapshot_name" {
  description = "(Optional) Name of a snapshot from which to restore data into the new node group."
  type        = string
  default     = null
}

variable "apply_immediately" {
  description = "(Optional) Specifies whether any modifications are applied immediately, or during the next maintenance window."
  type        = bool
  default     = false
}

variable "family" {
  description = "(Required) The family of the ElastiCache parameter group."
  type        = string
  default     = "redis5.0"
}

variable "tags" {
  description = "A mapping of tags to assign to all resources."
  type        = map(string)
  default     = {}
}

variable "cluster_mode_enabled" {
  description = "(Required) Create a native redis cluster. automatic_failover_enabled must be set to true. Cluster Mode documented below. Only 1 cluster_mode block is allowed."
  type        = bool
  default     = false
}

variable "cluster_mode_replicas_per_node_group" {
  description = "(Required) Number of replica nodes in each node group. Valid values are 0 to 5. Changing this number will force a new resource"
  type        = number
  default     = 0
}

variable "cluster_mode_num_node_groups" {
  description = "(Required) Number of node groups (shards) for this Redis replication group. Changing this number will trigger an online resizing operation before other settings modifications"
  type        = number
  default     = 0
}

variable "cluster_size" {
  description = "(Required) Number of nodes in cluster. *Ignored when `cluster_mode_enabled` == `true`*"
  type        = number
  default     = 1
}

variable "availability_zones" {
  description = "(Optional) A list of EC2 availability zones in which the replication group's cache clusters will be created. The order of the availability zones in the list is not important."
  type        = list(string)
  default     = []
}

variable "parameter" {
  type = list(object({
    name  = string
    value = string
  }))
  default     = []
  description = "A list of parameters to apply. Note that parameters may differ from one Redis family to another"
}

variable "auto_minor_version_upgrade" {
  description = "(Optional) Specifies whether a minor engine upgrades will be applied automatically to the underlying Cache Cluster instances during the maintenance window."
  type        = bool
  default     = true
}

variable "multi_az_enabled" {
  description = "(Optional) Specifies whether to enable Multi-AZ Support for the replication group."
  type        = bool
  default     = false
}
